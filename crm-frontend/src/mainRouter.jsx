import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import Homepage from "./Component/Admin/homepage";
import Login from "./Component/login/login";
import AddProject from "./Component/Admin/AddProject";
import ProjectSearch from "./Component/Admin/ProjectSearch";
import Addevent from "./Component/Admin/AddEvent";
import Addemployee from "./Component/Admin/ManageEmploye";
import ProjectDetail from "./Component/Admin/ProjectDetail";
import ProjectList from "./Component/Common/ProjectList";
import Report from "./Component/Reports/piechart";
import Navigation from "./Component/navbar/Navigation"
import LeadsReport from "./Component/Reports/LeadsReports";
import Viewemployee from "./Component/Admin/Viewemployee";
import ProfileMange from "./Component/Common/Profilemanage";
import Filterpage from "./Component/Admin/FilterLeads";
import PrivateRoute from "./Component/Routes/PrivateRoutes";
import AssignTarget from "./Component/Admin/AssignTarget";
import Dashboard from "./Component/Salesperson/Dashboard";
import EmployeeDetail from "./Component/Admin/EmployeeDetail";
import SignedOutLinks from "./Component/Common/SignedOutLinks";

class mainRouter extends Component {
 
  render() {
    return (
      <Switch>
        <PrivateRoute path="/home" exact component={Homepage} />
        <PrivateRoute path="/addemployee" exact component={Addemployee} />
        <PrivateRoute path="/project/:id" exact render={(props) => <ProjectDetail {...props} />} />
        <PrivateRoute path="/addproject" exact component={AddProject} />
        <PrivateRoute path="/project" exact component={ProjectSearch} />
        <PrivateRoute path="/Addevent" exact component={Addevent} />
        <PrivateRoute path="/Viewprojectlist" exact component={ProjectList} />
        <PrivateRoute path="/reports" exact component={Report} />
        <PrivateRoute path="/viewemployee" exact component={Viewemployee} />
        <PrivateRoute path="/Leadsreports" exact component={LeadsReport} />
        <PrivateRoute path="/filterpage" exact component={Filterpage} />
        <PrivateRoute path="/profilemanage" exact component={ProfileMange} />
        <PrivateRoute path="/employee/:id" render={(props) => <EmployeeDetail {...props} />} />
        <PrivateRoute path="/dashboard-employee" component={Dashboard} />
        <PrivateRoute path="/assigntarget" component={AssignTarget} />
        <Route path="/Login"exact component={Login} />
        <Route path="/signedoutpage" exact component={SignedOutLinks} />
        
        <PrivateRoute path="/Navigation" component={Navigation} />
        
      </Switch>
    );
  }
}

export default withRouter(mainRouter);