import {handleResponse} from '../helpers/handleResponse';
export const projectService = {
    getAllProject,
    getProjectById,
    addProject,
    updateProjectById,
    getAllInventory,
    getInventoryById,
    addInventory,
    updateInventoryById,
    deleteInventoryById
}
const apiUrl="http://localhost:4200";
function getAllProject() {
    const requestOptions = { method: 'GET'};
    return fetch(`${apiUrl}/projects`, requestOptions).then(handleResponse);
}

function getProjectById(id) {
    const requestOptions = { method: 'GET'};
    return fetch(`${apiUrl}/assignproject?salespersonid=${id}`, requestOptions).then(handleResponse);
}

function addProject(body){

}

function updateProjectById(id,body){

}

function getAllInventory(){

}

function getInventoryById(id){

}

function addInventory(body){

}

function updateInventoryById(id,body){

}

function deleteInventoryById(id){

}