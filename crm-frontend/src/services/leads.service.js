import {handleResponse} from '../helpers/handleResponse';
export const leadsService = {
    getAllLeads,
    getLeadById,
    getLeadByStatus  
}
const apiUrl="http://localhost:4200";
function getAllLeads() {
    const requestOptions = { method: 'GET'};
    return fetch(`${apiUrl}/leads`, requestOptions).then(handleResponse);
}

function getLeadById(id) {
    const requestOptions = { method: 'GET'};
    return fetch(`${apiUrl}/leads?id=${id}`, requestOptions).then(handleResponse);
}

function getLeadByStatus(status) {
    const requestOptions = { method: 'GET'};
    return fetch(`${apiUrl}/assignproject?status=${status}`, requestOptions).then(handleResponse);
}
