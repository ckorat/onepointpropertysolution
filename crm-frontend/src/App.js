import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router,withRouter } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Switch, Route, Redirect } from "react-router-dom";
import Nav from './Component/navbar/Navigation';
import fire from './Component/config/fire';
import Header from './Component/Common/header';
import Footer from './Component/Common/footer';
import MainRouter from './mainRouter';
import Login from './Component/login/login';
import Charts from './Component/Reports/piechart';
import PageNotFound from './Component/Common/PageNotFound';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user:{}
    }
  }
  componentDidMount() {
    this.authListener();
  }
  authListener() {
    if(localStorage.getItem("login"))
    {
         this.setState({user:true})
    }else{
       this.setState({user:false})
    }
  }
  render() {
    return ( 
      <Router>
    <>      
    { this.state.user ?  
             <>
            <Header />
           
            <MainRouter />
          
            <Footer />
            <ToastContainer/>
            </>
           :
           <>
        
        <Login/>
           </>
    }
    </>
    
      </Router>
    );
  }
}

export default withRouter(App);
