import React, { Component } from "react";
import logo from '../../logo.svg';
import Nav from '../navbar/Navigation';
import Login from "../login/login";
export default class header extends Component {
  render() {
    return (
      <div id="header">
        <div className="container">
          <div id="logo">
            <img src={logo} alt="logo" />
            <h1>Concrete Realtor</h1>
            
          </div>
         
        </div>
        { localStorage.getItem("login") ?
                        <Nav/>
                        :
                        null
        }
        
      </div>
    );
  }
}
