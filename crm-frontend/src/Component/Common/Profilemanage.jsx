import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';



const ProfileManage = (props) => {


 const handleEdit=(e)=>
{

}
  return (
    <div className="container">
    <div className="row justify-content-center">
      <div className="col-md-10 col-sm-12">

    <Form className="form-style">
       <div className="title">
            <h1>Profile Manage</h1>
        </div>
      <Row form>
      <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail">EmployeeId</Label>
            <Input type="text" name="empid" id="exampleEmail" />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Input type="email" name="email" id="exampleEmail" />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleFirstname">Firstname</Label>
            <Input type="text" name="firstname" id="firstname" placeholder="Firstname" />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup>
        <Label for="exampleAddress">Lastname</Label>
        <Input type="text" name="lastname" id="lastname" />
      </FormGroup>
      <FormGroup>
        <Label for="exampleAddress2">Address</Label>
        <Input type="textarea" name="address2" id="exampleAddress2" placeholder="Apartment, studio, or floor"/>
      </FormGroup>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleCity">City</Label>
            <Input type="text" name="city" id="exampleCity"/>
          </FormGroup>
        </Col>
        <Col md={4}>
          <FormGroup>
            <Label for="exampleState">State</Label>
            <Input type="text" name="state" id="exampleState"/>
          </FormGroup>
        </Col>
        <Col md={2}>
          <FormGroup>
            <Label for="Password">Password</Label>
            <Input type="password" name="password" id="password"/>
          </FormGroup>  
        </Col>
      </Row>
      <FormGroup>
             <button class="btn btn-primary mb-2" onClick={e=>{handleEdit(e) }} >Edit</button>
          </FormGroup>  
    </Form>
    </div>
  </div>
    </div>
  );
}

export default ProfileManage;