import React from 'react';
import {
    NavItem,
    Nav,
    DropdownMenu,
    DropdownItem,
    DropdownToggle,
    UncontrolledDropdown
} from 'reactstrap';
import {NavLink} from 'react-router-dom';
const SignedInAdminLinks = () => {
        return (
            <Nav className = "mr-auto" navbar >
            <NavItem>
            <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav>Project</DropdownToggle>
            <DropdownMenu>
                <DropdownItem>
                    <NavLink to="/project">View Project</NavLink>
                </DropdownItem>
                <DropdownItem>
                    <NavLink to="/addproject">Create Project</NavLink>
                </DropdownItem>
                
            </DropdownMenu>
            </UncontrolledDropdown>
            </NavItem>
            <NavItem><NavLink to="/logout">Logout</NavLink></NavItem>
            </Nav>
         );
}

export default SignedInAdminLinks;