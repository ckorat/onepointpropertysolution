import React from 'react';
import {
    Redirect
} from 'react-router-dom';

const SignedOutLinks = () => {
    console.log("Before clearing "+localStorage.getItem("login"))
    localStorage.clear();
    console.log("After clearing"+localStorage.getItem("login"));
        return (
            <div>
                 <Redirect to="/Login"></Redirect>
            </div>
           
         );
}

export default SignedOutLinks;