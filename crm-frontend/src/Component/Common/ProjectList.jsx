import React from 'react';
import { Table, Button, Container, Row, Col, Card, CardBody } from 'reactstrap'
import { Link } from 'react-router-dom'
const ProjectList = ({ projects }) => {
    return (
        <Container>
            <Row>
                <Col>
                    <Card>
                        <CardBody>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project Title</th>
                                        <th>Start Date</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {projects.length > 0 ?projects && projects.map((project, i) => {
                                        return (
                                        <tr key={project.id}>
                                           <th scope="row">{i+1}</th>
                                            <td>{project.name}</td>
                                            <td>{project.startDate}</td>
                                            <td>{project.type}</td>
                                            <td><Button outline>Edit</Button>
                                            <Link className="btn btn-outline-info"  to={`/project/${project.id}`}>View</Link>
                                            </td>
                                        </tr>
                                        )
                                    })
                                    :<h3>No Project Found</h3>
                                    }
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default ProjectList;