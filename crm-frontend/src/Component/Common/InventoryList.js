import React from 'react';
import {Table, Button, Container, Row, Col, Card, CardBody} from 'reactstrap';

const InventoryList = ({inventories,onEdit,onDelete}) => {
    
    return ( 
    <>
    <Container>
            <Row className="justify-content-center">
                <Col md={10}>
                    <Card>
                        <CardBody>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Units</th>
                                        <th>Carpet Area</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {inventories && inventories.map((inventory,i) => {
                                        return (
                                        <tr key={inventory.id}>
                                           <th scope="row">{i+1}</th>
                                            <td>{inventory.type}</td>
                                            <td>{inventory.totalUnits}</td>
                                            <td>{inventory.carpetarea}</td>
                                            <td>{inventory.description}</td>
                                            <td><Button outline id={i} name="edit"  onClick={(e)=>onEdit(e)}>Edit</Button>
                                            <Button className="ml-2" outline id={i} color="danger" onClick={(e)=>onDelete(e)}>Delete</Button></td>
                                        </tr>
                                        )
                                    })
                                    }
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    </> 
    );
}
 
export default InventoryList;