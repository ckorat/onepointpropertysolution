import React from 'react';
import { Container } from 'reactstrap';
const LeadForm = () => {
    return ( 
        <>
        <Container>
        <Row className="justify-content-center">
                        <Col md={6}>
                            <Card>
                                <CardBody>
                                <CardTitle><h2>Lead Form</h2></CardTitle>
                            <Form>
                                <FormGroup>
                                    <Label>First Name</Label>
                                    <Input type="text" name="firstname" />
                                </FormGroup>
                               
                                <FormGroup>
                                <Label>Last Name</Label>
                                    <Input type="text" name="lastname" />
                                </FormGroup>
                               
                                <FormGroup>
                                    <Label>Contact</Label>
                                    <Input type="text" name="contact" />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Lead Channel</Label>
                                    <Input type="select">
                                        <option>WalkIn</option>
                                        <option>Agent</option>
                                        <option>Online</option>
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                <Label>Project</Label>
                                    <Input type="select">
                                        <option>Prime Villa</option>
                                        <option>Sky Vista</option>
                                        <option>Avadh Utopia</option>
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Button type="submit" color="primary">Submit</Button>
                                    <Button type="reset">Clear</Button>
                                </FormGroup>
                            </Form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
        </Container>
        </>
     );
}
 
export default LeadForm;