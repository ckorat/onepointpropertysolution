import React from 'react';
import {
    Navbar,
    NavbarBrand
} from 'reactstrap';
import {Link} from 'react-router-dom';
import SignedInAdminLinks from './SignedInAdminLinks';
import SignedOutLinks from './SignedOutLinks';
const NavBar = () => {
        return (
            
            <Navbar color = "light"light expand = "md" >
            <NavbarBrand><Link to="/"> Concrete Realtor</Link> </NavbarBrand> 
            <SignedInAdminLinks />
            <SignedOutLinks />
            </Navbar>
         );
}

export default NavBar;