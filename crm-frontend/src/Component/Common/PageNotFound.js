import React from 'react';
import { Container, Row, Col, CardLink,CardBody,Card } from 'reactstrap';
const PageNotFound = () => {
    return ( 
        <>
        <Container>
            <Row className="justify-content-center">
                <Col>
                <Card>
                    <CardBody>
                        <h3>404 Page not found</h3>
                        Go to<CardLink>Home</CardLink>
                    </CardBody>
                </Card>
                </Col>
            </Row>
        </Container>
        </>
     );
}
 
export default PageNotFound;