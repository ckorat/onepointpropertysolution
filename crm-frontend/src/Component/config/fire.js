import firebase from 'firebase';
var firebaseConfig = {
    apiKey: "AIzaSyC4yQJaxjYhFDEHxS2Km03tLJvpzckv9eI",
    authDomain: "realestate-85901.firebaseapp.com",
    databaseURL: "https://realestate-85901.firebaseio.com",
    projectId: "realestate-85901",
    storageBucket: "realestate-85901.appspot.com",
    messagingSenderId: "43739143640",
    appId: "1:43739143640:web:35751b14d0615485ff26ad",
    measurementId: "G-LG9JNZ3HMQ"
};
const fire=firebase.initializeApp(firebaseConfig);
export default fire;