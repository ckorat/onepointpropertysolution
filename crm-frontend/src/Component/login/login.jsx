import React , { Component } from "react";
import fire from "../config/fire";
import { Redirect,Link } from "react-router-dom";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from "axios";
import {toast } from 'react-toastify';
const initialstate={
        role:"",
        email: "",
        password : "",
        emailError:"",
        passwordError:""
}

class Login extends Component{
constructor(props)
{
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
   
    this.state={
        role :"",
        email : "",
        password : "",
        islogged:false,
        emailError:"",
        passwordError:""
    }
}
login(e){
    e.preventDefault();
    console.warn(this.state);
    fetch("http://localhost:4200/login?email="+this.state.email)
        
      .then((result) => {  
          result.json().then((res)=> {
              console.warn("res",res)

              if(res.length>0)
              {
                localStorage.setItem("login",JSON.stringify(res[0]))
                this.setState({islogged:true})
                toast.success("Successfully welcome to page");
              }else{

                toast.error(" not got record");
              }
          })

      }) 
}
validate=()=>{

    let emailError="";
    let passwordError="";
    if(!this.state.email.includes("@")){
      emailError="Invalid Email";
    }
    if(this.state.password.length >=5){
        passwordError="password must 6 character long";
    }
    if(emailError ||passwordError ){
        this.setState({emailError});
        this.setState({passwordError});
        return false;
    }
    return true;
}
handleChange(e){
  
        this.setState({
            [e.target.name] : e.target.value
        })
    
       
    }
render()
{
    if(this.state.islogged)
    {
        return <Redirect to="/home" />
    }
    return(
       
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 col-sm-8">
                      
                        <Form className="form-style" >
                            <div className="panel">
                            
                                <div className="title">
                                    <h1>Login</h1>
                                </div>

                                <div className="col-lg-12 well">
                                    <div className="col-sm-12">
                                        <div className="form-group row">
                                            <Label className="col-sm-3 col-form-label text-left" for="exampleSelect">Select Role:</Label>
                                            <div class="col-sm-9">
                                                <Input type="select" name="role" id="exampleSelect"  value={this.state.role}  onChange={this.handleChange}  className="form-control">
                                                    <option value="admin">Admin</option>
                                                    <option value="Saleperson">Saleperson</option>
                                                    <option value="Agents">Agents</option>
                                                </Input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 well">
                                    <div className="col-sm-12">
                                        <div className="form-group row">
                                            <Label className="col-sm-3 col-form-label text-left" for="Email">Email:</Label>
                                               <div class="col-sm-9">
                                                    <input
                                                            className="form-control"
                                                            type="email"
                                                            id="email"
                                                            name="email"
                                                            placeholder="enter email address"
                                                            onChange={this.handleChange}
                                                            value={this.state.email}
                                                    />
                                                </div>
                                         </div>
                                         <div style={{ fontSize:12,color:"red"}}>{this.state.emailError} </div>
                                     </div>
                                </div>
                                
                                 <div className="col-lg-12 well">
                                    <div className="col-sm-12">
                                        <div className="form-group row">
                                            <Label  className="col-sm-3 col-form-label text-left" for="password">Password:</Label>
                                            <div class="col-sm-9">
                                                <input
                                                    className="form-control"
                                                    name="password"
                                                    type= "password"
                                                    onChange={this.handleChange}
                                                    id="password"
                                                    placeholder="enter password"
                                                    value={this.state.password}
                                                    />
                                             </div>
                                       </div >
                                       <div style={{ fontSize:12,color:"red",paddingLeft:"3px;"}}>{this.state.passwordError}</div> 
                                    </div>
                                </div>                       
                        <div className="text-center">
                            <button onClick={this.login} className="btn btn-primary mt-2 mb-2">Login</button>
                            <p className="mb-0"><Link to="/login/reset">Forgot password?</Link></p>
                        </div>                        
                       </div>  
                    </Form>   
                    </div>
                </div>
            </div>
    )
  }
 }
export default Login;