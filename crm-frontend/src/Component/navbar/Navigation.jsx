import signedoutpage from '../Common/SignedOutLinks';
import React, { useState } from 'react';
import { Redirect,Link } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import { useEffect } from 'react';

const Navigation = (props) => {

  const [isOpen, setIsOpen] = useState(false);
  const [json, setjson] = useState(false);
  const[role,setrole]=useState('');
  const toggle = () => setIsOpen(!isOpen);
  const getdata = () => {
 
       const temp=JSON.parse(localStorage.getItem("login"));
       temp ? setrole(temp.role) : setrole("");
      

     }
  useEffect(()=>{
    getdata();
  },[]);
  
  let disapRoleBasedNavLink; 
  if(role==="Admin"){
    disapRoleBasedNavLink=<Navbar expand="md">
    <NavbarBrand href="/home">Home</NavbarBrand>
    <NavbarToggler onMouseMove={toggle} />
    <Collapse isOpen={isOpen} navbar>
      <Nav className="mr-auto" navbar>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Events
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/AddEvent">Add Events</NavLink>
            </DropdownItem>
            <DropdownItem divider />
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavItem>
          <NavLink href="/reports">Reports</NavLink>
        </NavItem>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Projects
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/addproject">Add Projects</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              <NavLink href="/viewproject">View Projects</NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Employee
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/addemployee">Add Employee</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              <NavLink href="/viewemployee">View Employee</NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Target
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/addtarget">Add Tareget</NavLink>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              <NavLink href="/viewtarget">View Target</NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavLink href="/filterpage">SearchLeads</NavLink>
      </Nav>
      <NavLink href="/signedoutpage">Logout</NavLink>
    </Collapse>
  </Navbar>;
  }
  else if(role==="Saleperson"){
    disapRoleBasedNavLink=<Navbar expand="md">
    <NavbarBrand href="/dashboard-employee">Home</NavbarBrand>
    <NavbarToggler onMouseMove={toggle} />
    <Collapse isOpen={isOpen} navbar>
      <Nav className="mr-auto" navbar>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Leads
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/assigntarget">AssignTarget</NavLink>
            </DropdownItem>
            <DropdownItem divider />
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavLink href="/profilemanage">ProfileManage</NavLink>
      </Nav>
      <NavLink href="/login">Logout</NavLink>
    </Collapse>
  </Navbar>;
  }
  else if(role==="Agent"){
    disapRoleBasedNavLink=<Navbar expand="md">
    <NavbarBrand href="/dashboard-employee">Home</NavbarBrand> 
     <NavbarToggler onMouseMove={toggle} />
    <Collapse isOpen={isOpen} navbar>
      <Nav className="mr-auto" navbar>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Leads
         </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <NavLink href="/addleads">Addleads</NavLink>
            </DropdownItem>
            <DropdownItem divider />
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavLink href="/profilemanage">ProfileManage</NavLink>
      </Nav>
      <NavLink href="/Login">Logout</NavLink>
    </Collapse>
  </Navbar>;
  }else
  {
    
  }
  return (
    
    <div>
      {disapRoleBasedNavLink}
    </div>

  );
}

export default Navigation;