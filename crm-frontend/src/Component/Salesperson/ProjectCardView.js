import React from 'react';
import { CardColumns,Button,Card,CardBody,CardText,CardTitle,CardSubtitle,CardImg } from 'reactstrap';
const ProjectCardView = ({projects,onView}) => {
    return ( 
        <>
        <CardColumns>
            {projects?
                projects.map((project,i) => 
                    <Card key={i+1}>
                        <CardImg top width="100%" src="https://attendantdesign.com/wp-content/uploads/2017/08/slide-lg-2-1.jpg" alt="Card image cap" />
                        <CardBody>
                            <CardTitle><h3>{project.name}</h3></CardTitle>
                            <CardSubtitle>{project.type}</CardSubtitle>
                            <CardText>{project.description}</CardText>
                            <Button id={project.id} onClick={(e)=>onView(e)}>View</Button>
                        </CardBody>
                    </Card>
                )
                :<h3>No Project Assigned</h3>

            }
        </CardColumns>
        </>
     );
}
 
export default ProjectCardView;