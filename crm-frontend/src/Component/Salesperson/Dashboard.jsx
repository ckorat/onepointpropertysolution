import React, { Component } from 'react';
import { Container,Row,Col, Spinner } from 'reactstrap';
import ProjectCardView from './ProjectCardView';
import {projectService} from '../../services/project.service'
class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            projects:'',
            assignedproject:'',
            projectList:'',
            isLoading:0
         }
    }
    async componentDidMount(){
        this.setState({isLoading:1});
      await projectService.getAllProject().then(data => this.setState({projects:data}))
      await projectService.getProjectById(2).then(data => this.setState({assignedproject:data}))
       const list=[];
       this.state.assignedproject.map(
           item=>{
               this.state.projects.map(
                   project=>{
                       if(item.projectid === project.id) 
                        list.push(project);
               })
           }
       )
       this.setState({projectList:list,isLoading:0})
    }
    handleView=()=>{

    }
    render() { 
        
        return (
            <>
            <Container>
                <Row>
                    <Col><h2>Projects</h2></Col>
                </Row>
                <Row className="justify-content-center">
                    {this.state.isLoading===0? this.state.projectList.length > 0 &&
                        <ProjectCardView projects={this.state.projectList} onView={this.handleView}/>
                    :   <Spinner></Spinner>
                    }
                </Row>
            </Container>
            </>
         );
    }
}
 
export default Dashboard;