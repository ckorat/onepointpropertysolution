import React, { Component } from 'react'  
import axios from 'axios';  
import { Pie } from 'react-chartjs-2';  

export class Piechart extends Component {  
        constructor(props) {  
                super(props);  
                this.state = { Data: {} };  
        }  
        componentDidMount() {  
                axios.get(`http://localhost:4200/addemployee`)  
                        .then(res => {  
                                console.log(res);  
                                const ipl = res.data;  
                                const calsaleperson=0;
                                const calagents=0;

            
                                let Employee = ['Salesperson', 'Agent']; 
                                let runscore = [10, 30] ;
                              
                                this.setState({  
                                        Data: {  
                                                labels: Employee,  
                                                datasets: [  
                                                        {  
                                                                label: 'Employee',  
                                                                data: runscore,  
                                                                backgroundColor: [  
                                                                     
                                                                        "Blue",  
                                                                        "Red"  
                                                                ]  
                                                        }  
                                                ]  
                                        }  
                                });  
                        })  
        }  
        render() {  
                return (  
                        <div> 
                                <Pie  
                                      data={this.state.Data}  
                                        options={{ maintainAspectRatio: false }} />  
                        </div>  
                )  
        }  
}  
export default Piechart ;