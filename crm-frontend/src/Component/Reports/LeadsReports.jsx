import React, { Component } from 'react'  
import axios from 'axios';  
import { Bar } from 'react-chartjs-2';  

export class LeadsReports extends Component {  
        constructor(props) {  
                super(props);  
                this.state = { Data: {} };  
        }  
        componentDidMount() {  
                axios.get(`http://localhost:4200/leads`)  
                        .then(res => {  
                                //console.log(res);  

                                const ipl = res.data; 
                                console.log(res.data) 
                                const calsaleperson=0;
                                const calagents=0;

            
                                let Employee = ['Leads']; 
                                let runscore = [] ;
                                runscore.push(res.data.length);

                                // ipl.array.forEach(element => {
                                //      runscore.push(element.creationdate);
                                // });
                                this.setState({  
                                        Data: {  
                                                labels: Employee,  
                                                datasets: [  
                                                        {  
                                                                label: 'Employee',  
                                                                data: runscore,  
                                                                backgroundColor: [  
                                                                     
                                                                        "Blue",  
                                                                        "Red"  
                                                                ]  
                                                        }  
                                                ]  
                                        }  

                                });  
                        })  

        }  
        render() {  
                return (  
                        <div> 
                                <Bar
                                      data={this.state.Data}  
                                        options={{ maintainAspectRatio: false }} />  
                        </div>  
                )  
        }  
}  
export default LeadsReports ;