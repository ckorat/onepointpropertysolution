import React, { Component } from 'react';
import { Button, Label, Spinner, Row, Col, CardTitle,CardBody,CardHeader, Card, CardImg, CardText, Container } from 'reactstrap';
import InventoryList from '../Common/InventoryList';
import InventoryForm from './InventoryForm';

class ProjectDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: '',
      inventories: '',
      inventory:{},
      isEditing:false,
      modal:false,
      index:null,
      isLoading:0
    }
  }
  async componentDidMount() {
    this.setState({isLoading:1});
   await fetch(`http://localhost:3001/projects/${this.props.match.params.id}`)
      .then(response => response.json())
      .then(data => this.setState({ project: data }))
    await this.fetchInventories();
    this.setState({isLoading:0});
  }

  fetchInventories= async()=>{
   await fetch(`http://localhost:3001/inventory?project=${this.props.match.params.id}`)
    .then(response => response.json())
    .then(data => this.setState({ inventories: data }))
  }

  
  onEdit=(e)=>{
    e.preventDefault();
   
    this.setState({
      inventory:this.state.inventories[e.target.id],
      index:this.state.inventories[e.target.id].id

    })
    this.state.isEditing=true;
    this.setState({modal:!this.state.modal});
    
}
onDelete= async(e)=>{
  this.setState({isLoading:1});
  const inventoryId=this.state.inventories[e.target.id].id;
 await fetch(`http://localhost:3001/inventory/${inventoryId}`,{method:'DELETE'})
  .then(response=>response.json())
  await this.fetchInventories()
  this.setState({isLoading:0});
}

  toggle = () =>{ 
    this.setState({modal:!this.state.modal});
  }
  
  handleSubmit =  async(inventory,e) => {
    
    inventory.project=parseInt(this.props.match.params.id);
    this.setState({isLoading:1});
   await fetch('http://localhost:3001/inventory',
       {headers:{'Content-Type':'application/json'}, method: 'POST', body: JSON.stringify(inventory) })
       .then(response=> response.json())
       await this.fetchInventories();
       this.setState({modal:!this.state.modal});

       e.preventDefault(); 
       this.setState({isLoading:0});
  }
  handleSave = async (inventory,e) => {
    inventory.project=parseInt(this.props.match.params.id);
    this.setState({isLoading:1});
     await fetch(`http://localhost:3001/inventory/${this.state.index}`,
       {headers:{'Content-Type':'application/json'}, method: 'PUT', body: JSON.stringify(inventory) })
       .then(response=> response.json())
      await this.fetchInventories();
       this.setState({modal:!this.state.modal});
       this.setState({isLoading:0});
      e.preventDefault();
  }
  render() {
    const { name, type, startDate, address, description } = this.state.project;

    return (
    <Container>
      <Row className="justify-content-center">
        <Col md={10}>
          
          <Card>
            <CardTitle><h1>Project Detail</h1></CardTitle>
            <CardHeader><h3>Project Name:{' ' + name}</h3></CardHeader>
            <CardImg src={'https://attendantdesign.com/wp-content/uploads/2017/08/slide-lg-2-1.jpg'} alt="project poster" height="500px" ></CardImg>
            <CardBody>
              <CardText><Label>Type:</Label>{' ' + type}</CardText>
              <CardText><Label >Start Date:</Label>{' ' + startDate}</CardText>
              <CardText> <Label >Address:</Label>{' ' + address}</CardText>
              <CardText><Label >Description:</Label>{' ' + description}</CardText>

            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col className="text-center"><Button color="success" outline  onClick={this.toggle}>Add Inventory</Button></Col>
      </Row>

    { 
    this.state.modal &&
      <InventoryForm handleSave={this.handleSave} handleSubmit={this.handleSubmit} inventory={this.state.inventory} toggle={this.toggle} modal={this.state.modal} isEditing={this.state.isEditing}  />
    }
         
      <Row className="justify-content-center">
        <Col>
        {this.state.isLoading===0?
          this.state.inventories.length > 0 ?
            <InventoryList onEdit={this.onEdit} onDelete={this.onDelete} inventories={this.state.inventories} toggle={this.toggle} errors={this.state.errors} onChange={this.onChange} />

          : <h3 className="mt-4">No Inventories</h3>
          :<Spinner></Spinner>
        }
        </Col>
      </Row>
    </Container>
    );
  }
}

export default ProjectDetail;