import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col ,Card, CardBody, CardTitle } from 'reactstrap'
import FormError from '../Common/FormError';
class AddProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                name: '',
                type: '',
                startDate: '',
                address: '',
                description: '',
                broucher: '',
                projectimage: ''
            },
            errors: {}
        }
    }
    onChange = (e) => {
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })

    }

    onChangeFile = (e) => {

        this.setState({
            data: { projectimage: e.target.files[0] }
        })

    }
    onCreate = (event) => {
        const errors = this.validate(this.state.data);
        this.setState({ errors })

        fetch('http://localhost:3001/projects', {
            method: "POST",
            body: JSON.stringify(this.state)
        })
            .then(response => response.json())

        event.preventDefault();
    }
    validate = (data) => {
        const errors = {};
        if (!data.name) errors.name = "*please enter project name";
        if (!data.type) errors.type = "*please select project type";
        if (!data.startDate) errors.startDate = "*select date";
        if (!data.address) errors.address = "*please enter address";
        if (!data.description) errors.description = "*please enter some project description";
        return errors;
    }

    render() {
        return (
            <Container>
                <Row className="justify-content-center">
                    <Col md={8}>
                        <Card>
                            <CardBody>
                               <CardTitle> <h2>Add New Project</h2> </CardTitle>
                                <Form onSubmit={this.onCreate}>
                                    <FormGroup>
                                        <Label>Name:</Label>
                                        <Input invalid={!!this.state.errors.name} type="text"
                                            name="name"
                                            placeholder="enter project name"
                                            onChange={this.onChange}></Input>
                                        {this.state.errors.name && <FormError text={this.state.errors.name} />}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Project Type</Label>
                                        <Input invalid={!!this.state.errors.type} type="select"
                                            name="type"
                                            onChange={this.onChange}>
                                            <option value="">--select--</option>
                                            <option>Commercial</option>
                                            <option>Residential</option>
                                        </Input>
                                        {this.state.errors.type && <FormError text={this.state.errors.type} />}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Date</Label>
                                        <Input invalid={!!this.state.errors.startDate} type="date" name="startDate" onChange={this.onChange}></Input>
                                        {this.state.errors.startDate && <FormError text={this.state.errors.startDate} />}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Address</Label>
                                        <Input invalid={!!this.state.errors.address} type="textarea" name="address" onChange={this.onChange}></Input>
                                        {this.state.errors.address && <FormError text={this.state.errors.address} />}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Description</Label>
                                        <Input invalid={!!this.state.errors.description} type="textarea" name="description" onChange={this.onChange}></Input>
                                        {this.state.errors.description && <FormError text={this.state.errors.description} />}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Project Docs</Label>
                                        <Input type="file" name="file" onChange={this.onChangeFile}></Input>
                                    </FormGroup>
                                    <FormGroup>
                                        <Button type="submit" color="primary" >Submit</Button>
                                        <Button type="reset">Clear</Button>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>>
            </Container>
        );
    }
}

export default AddProject;