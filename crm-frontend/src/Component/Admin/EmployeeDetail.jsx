import React, { Component } from 'react';
import { Container, Card, CardHeader, CardTitle, CardBody, CardImg, Row, Col, Button, Spinner } from 'reactstrap';
import Select from 'react-select';
import AssignedProjectList from './AssignedProjectList';
import FormError from '../Common/FormError';

class EmployeeDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: {},
            projectList: [],
            assignedprojects: {},
            errors: {},
            isLoading: 0,
            selectedoption: null
        }
    }
    async componentDidMount() {
        await fetch('http://localhost:3001/projects')
            .then(response => response.json(), this.setState({ isLoading: 1 }))
            .then(data => this.setState({ projects: data }))


        await fetch(`http://localhost:3001/assignproject?salespersonid=${this.props.match.params.id}`)
            .then(response => response.json(), this.setState({ isLoading: 1 }))
            .then(data => this.setState({ assignedprojects: data }))
            .then(await this.fetchAssignedProjects())
        this.setState({ isLoading: 0 })

    }
    fetchAssignedProjects = async () => {
        let list = {}
        this.setState({ isLoading: 1 })
        await fetch(`http://localhost:3001/assignproject?salespersonid=${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => this.setState({ assignedprojects: data }))
            .then(list = this.state.projects.map((project) => {
                return { value: project.id, label: project.name, isDisabled: false }
            }))
            .then(this.setState({ projectList: list }))

        this.state.projectList.map((item) => {
            this.state.assignedprojects.map(project => {
                if (project.projectid === item.value)
                    item.isDisabled = true;
            })
        })
    }

    onHandleChange = (selectedoption) => {
        this.setState({ selectedoption });
    }
    onHandleAssign = async () => {
        if (this.state.selectedoption === null) {
            this.setState({ errors: { assignerror: 'please select projects' } });
        } else {
            this.setState({ isLoading: 1 });
            const d = new Date();
            const body = {
                salespersonid: parseInt(this.props.match.params.id),
                allocationdate: d.toISOString().split('T')[0],
                projectid : this.state.selectedoption.value
            }

            await fetch(`http://localhost:3001/assignproject`,
                {
                    headers: { 'Content-Type': 'application/json' },
                    method: 'POST',
                    body: JSON.stringify(body)
                }
            )
                .then(response => response.json())
            await this.fetchAssignedProjects();
            this.setState({ isLoading: 0, selectedoption: null, errors: {} });
        }

    }
    onDelete = async (e) => {
        this.setState({ isLoading: 1 })
        await fetch(`http://localhost:3001/assignproject/${e.target.id}`,
            { method: 'DELETE' })
            .then(response => response.json())
        await this.fetchAssignedProjects()
        this.setState({ isLoading: 0 })

    }
    render() {
        return (
            <>
                <Container >
                    <Row className="mt-3">
                        <Col sm="12" md={{ size: 8, offset: 2 }}><h3>Employee Detail</h3></Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12" md={{ size: 8, offset: 2 }}>
                            <Card>
                                <CardImg className="" top width="10px" height="300px" src={'https://pecb.com/conferences/wp-content/uploads/2017/10/no-profile-picture-300x216.jpg'}></CardImg>
                                <CardHeader>Employee Detail</CardHeader>

                                <CardTitle>Name:</CardTitle>

                                <CardBody>

                                    All Employee Detail</CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="12" md={{ size: 8, offset: 2 }}>


                            <Select
                                options={this.state.projectList}
                                isLoading={this.state.isLoading}
                                value={this.state.selectedoption}
                                onChange={this.onHandleChange}
                            />
                            {this.state.errors.assignerror && <FormError text={this.state.errors.assignerror} />}
                            <Button onClick={this.onHandleAssign}>Assign</Button>
                        </Col>

                    </Row>
                    <Row>
                        <Col className="mb-3" sm="12" md={{ size: 8, offset: 2 }}><h3>Assigned Projects</h3></Col>
                    </Row>
                    <Row>
                        <Col >

                            {
                                this.state.isLoading === 0 ?
                                    this.state.assignedprojects.length > 0 && this.state.projects.length > 0 ?
                                        <AssignedProjectList onDelete={this.onDelete} projects={this.state.projects} assignedprojects={this.state.assignedprojects} />
                                        : <div className="text-center"> <h3>No Project </h3></div>
                                    : <div className="text-center"><Spinner></Spinner></div>
                            }
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default EmployeeDetail;