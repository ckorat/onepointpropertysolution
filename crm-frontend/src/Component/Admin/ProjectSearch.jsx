import { Input, Button, InputGroup, Container, Row, Col, Spinner } from "reactstrap";
import ProjectList from "../Common/ProjectList";
import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { projectService } from '../../services/project.service';
class ProjectSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: null,
            projects: null,
            isLoading: 0
        }

    }
    async componentDidMount() {
        this.setState({ isLoading: 1 });
        await projectService.getAllProject().then(data => this.setState({ projects: data }))
        this.setState({ isLoading: 0 });
    }
    handleSearch = async (event) => {
        this.setState({ isLoading: 1 });
        await fetch(`http://localhost:3001/projects?q=${this.state.search}`)
            .then(response => response.json())
            .then(data => this.setState({ projects: data }))
        this.setState({ isLoading: 0 });
    }
    render() {
        return (<>
            <Container>
                <Row className="justify-content-center">
                    <Col md={8}>
                        <InputGroup>
                            <Input type="search" name="search" placeholder="search project" onChange={(event) => { this.setState({ search: event.target.value }) }}></Input>
                            <Button outline color="primary" onClick={this.handleSearch}>Search</Button>
                        </InputGroup>
                    </Col>
                </Row>
                <Row className="justify-content-center mt-3">
                    <Link to="/addproject"><Button className="mb-3" outline color="success">Create Project</Button></Link>
                </Row>
                <Row className="justify-content-center">
                    {this.state.isLoading === 0 ?
                        this.state.projects ?
                            <ProjectList projects={this.state.projects} />
                            : <h3>Add Project</h3>
                        : <Spinner></Spinner>
                    }
                </Row>
            </Container>
        </>
        );
    }
}

export default ProjectSearch;