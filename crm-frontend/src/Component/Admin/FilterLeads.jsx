
import React, { Component } from 'react';
import { leadsService } from '../../services/leads.service';
import { Input, Button, InputGroup, Container, Row, Col, Spinner,FormGroup,Label } from "reactstrap";
import PageNotFound from '../Common/PageNotFound';



class FilterLeads extends Component {

  constructor(props) {
    super(props);
    this.state = {
        search: null,
        Leads: null,
        isLoading: 0
    }

}
async componentDidMount() {
    this.setState({ isLoading: 1 });
    await  leadsService.getLeadByStatus().then(data => this.setState({ Leads: data }))
    this.setState({ isLoading: 0 });
}
handleSearch = async (event) => {
    this.setState({ isLoading: 1 });
    await fetch(`http://localhost:4200/leads?q=${this.state.search}`)
        .then(response => response.json())
        .then(data => this.setState({ Leads: data }))
    this.setState({ isLoading: 0 });
}
render() {
    return (<>
        <Container>
            <Row className="justify-content-center">
            <Label for="exampleSelect" sm={2}>Select-Status</Label>
                <Col md={10}>
                    <InputGroup>
                    <Input type="select" name="select" id="exampleSelect">
                          <option>New</option>
                          <option>Created</option>
                          <option>Dead</option>
                          <option>Progress</option>
                         
                        </Input>     
                        <Button outline color="primary" onClick={this.handleSearch}>Search</Button> 
                    </InputGroup>
                </Col>
            </Row>
            <Row className="justify-content-center">
                
            </Row>
        </Container>
    </>
    );
}
}
export default FilterLeads;