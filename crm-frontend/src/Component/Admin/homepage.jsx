import React, { Component } from "react";
import axios from 'axios';  

export default class homepage extends Component {
 
  constructor(props) {  
    super(props);  
    this.state = { Data: {},count:'0',leadscount:'0',eventcount:0};
    
  
}  
    componentDidMount() {  
    console.log("componentDidMount calling");
      axios.get(`http://localhost:4200/projects`)  
              .then(res => {  
                        this.setState({
                          count:res.data.length
                        })
                        console.log("projects"+this.state.count);
              })  
      axios.get(`http://localhost:4200/addevent`)  
              .then(res => { 
                this.setState({
                  eventcount:res.data.length
                }) 
                      
             })
      axios.get(`http://localhost:4200/leads`)  
             .then(res => {  
              this.setState({
                leadscount:res.data.length
              }) 
            })     
}  
  render() {
    
    return (
      
      <div className="homepage">
         
         <div className="main">
       
         </div>
        
        <div id="banner">
          <div className="container" />
                
           
        </div>
        <div id="page">
         
          <div id="marketing" className="container">
            <div className="row">
              
                <div className="col md-4">

                    <div className="card-deck">
                    <div className="card" href="/projects">
                      <div className="card-header">
                        <h4>Projects</h4>
                      </div>
                      <div className="card-body">
                        <div className="inline-col2">
                        <div className="card-text inner">Total Projects<h1 className="green">{this.state.count}</h1>
                          </div>
                        </div>
                      </div>
                    </div>     
                  </div>
                </div>
                <div className="col md-4">
                    
                <div className="card-deck">
                    <div className="card" href="/projects">
                      <div className="card-header">
                        <h4>Leads</h4>
                      </div>
                      <div className="card-body">
                        <div className="inline-col2">
                        <div className="card-text inner">Total Leads <h1 className="green">{this.state.leadscount}</h1>
                          </div>
                        </div>
                      </div>
                    </div>     
                  </div>
                </div>
                <div className="col md-4">
                  <div className="card-deck">
                      <div className="card" href="/projects">
                        <div className="card-header">
                          <h4>Events</h4>
                        </div>
                        <div className="card-body">
                          <div className="inline-col2">
                          <div className="card-text inner">Total Events<h1 className="green">{this.state.eventcount}</h1>
                            </div>
                          </div>
                        </div>
                      </div>     
                    </div>
                  </div>
            </div>
          </div>
      </div>
      </div>
    );
  }
}
