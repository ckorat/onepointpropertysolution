import React from 'react';
import { Form } from 'reactstrap';

import axios from "axios";
import { useEffect, useState } from 'react';

const Addevent = (props) => {

  const onDrop = (picture) => {


    this.setState({
      pictures: this.state.pictures.concat(picture),

    });
  }

  
  const [eventlist, seteventlist] = useState([]);
  const [editing, setEditing] = useState(false);
  useEffect(async () => {
    const result = await axios(
      'http://localhost:4200/addevent',
    );
   
    seteventlist(result.data);

  }, []);

  const [Event, setEvent] = useState({id:'', name: '', EventDate: '', type: '', projectId: '', description: '', description: '',address: '',status:'',file:'' });
  const handleEdit = (event,Eventdata) => {  
      event.preventDefault();
      setEditing(true);
      setEvent({id:Eventdata.id,name: Eventdata.name, EventDate: Eventdata.EventDate, type: Eventdata.type, projectId: Eventdata.projectId, description: Eventdata.description,address:Eventdata.address,status:Eventdata.status,file:Eventdata.file })
    
      
  };  

  const Setvalues=(e)=>{
    const [Event, setEvent] = useState({id:'', name: '', EventDate: '', type: '', projectId: '', description: '', description: '',address: '',status:'',file:'' });
    
  }
  const onChange = (e) => {
    e.persist();
    setEvent({ ...Event, [e.target.name]: e.target.value });
  }
  const handleSubmit=(e)=>{
    e.preventDefault();
    
    const data = {name: Event.name, EventDate: Event.EventDate, type: Event.type, projectId: Event.projectId, description: Event.description,address:Event.address,status:Event.status,file:Event.file };
    if(editing) {
      
      axios.put(`http://localhost:4200/addevent/${Event.id}` ,data)
      .then((result) => {  
       
        props.history.push('/addevent') 
      });  
    
    }else{
      axios.post('http://localhost:4200/addevent',data)
      .then((result) => {
        props.history.push('/addevent')
      });
    }
    
  }
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-10 col-sm-12">
          <Form className="form-style">
        <div className="panel">
          <div className="title">
            <h1>Add new Event </h1>
          </div>
          <div className="Event-form">
            <br />
            <div>
              <div className="col-lg-12 well">
                <div className="col-sm-12">
                  <div className="col-sm-12 form-group">
                    <label for="EventName">Event-Name:</label>
                    <input type="text" className="form-control" placeholder="EnterFirst-Name" name="name" onChange={onChange} defaultValue={Event.name} />
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="Eventdate">Event-date:</label>
                    <input type="date" className="form-control" name="EventDate" placeholder="Enterdate" defaultValue={Event.EventDate} onChange={onChange} />
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="Eventtype">Event-type:</label>
                    <input type="text" className="form-control" name="type" placeholder="Enter Eventtype" defaultValue={Event.type} onChange={onChange} />
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="projectid">Projectname:</label>
                    <select className="form-control" name="projectId"  value={Event.projectId} rows="5" onChange={e => onChange(e)}  >
                      <option defaultValue={101}>Building Home </option>
                      <option defaultValue={102}>Flats</option>
                      <option defaultValue={103}>Commerical </option>
                    </select>
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="description">description:</label>
                    <input type="textarea" className="form-control" name="description" placeholder="Enter description" defaultValue={Event.description} onChange={onChange} />
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="Adress">Address:</label>
                    <input type="textarea" className="form-control" name="address" placeholder="Enter address" defaultValue={Event.address} onChange={onChange} />
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="status">status:</label>
                    <select value={Event.status} name="status" className="form-control" name ="status"rows="5" onChange={e => onChange(e)}>
                      <option> Active </option>
                      <option> Inactive</option>
                    </select>
                  </div>
                  <div className="col-sm-12 form-group">
                    <label for="file">file:</label>
                    <div className="file-field">
                      <div className="btn btn-primary btn-sm">
                        <span>Choose file</span> &nbsp;&nbsp;
                            <input type="file" name="file" onChange={e => onChange(e)} />
                      </div>


                    </div>
                  </div>
                  <br />
                  <center className="mb-2">
                    <button className="btn btn-primary mb-2" onClick={handleSubmit}  >{editing ? 'Update' : 'Add'}</button>
                    &nbsp;
                    <button className="btn btn-secondary mb-2" onClick={Setvalues}>cancel</button>
                  </center>
                  <br />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="table-responsive">
        <table class="table table-striped" border="2">
          <thead>
              <tr className="table-primary">
                <th scope="col">EventId</th>
                <th scope="col">EventName</th>
                <th scope="col">Eventdate</th>
                <th scope="col">type</th>
                <th scope="col">Projectid</th>
                <th scope="col">description</th>
                <th scope="col">address</th>
                <th scope="col">status</th>
                <th scope="col">Image</th>
                <th scope="col">Edit</th>
              </tr>
          </thead>
            <tbody>
              {eventlist.map(data =>
                  <tr key={data.id} >
                    <td>{data.id}</td>
                    <td>{data.name}</td>
                    <td>{data.EventDate}</td>
                    <td>{data.type}</td>
                    <td>{data.projectId}</td>
                    <td>{data.description}</td>
                    <td>{data.address}</td>
                    <td>{data.status}</td>
                    <td>{data.file}</td>
                    <td><button class="btn btn-primary mb-2" onClick={e=>{handleEdit(e,data) }} >Edit</button></td>
                  </tr>
              )}
            </tbody>
          </table>
        </div>
    </Form>
        </div>
      </div>
    </div>
  );
}
export default Addevent;

