import React, { Component } from 'react';
import { Container, Row, Col, FormGroup,Input,Label,Form,Card,CardBody,CardTitle, Button } from 'reactstrap';
class AssignTarget extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <>
                <Container>
                    <Row className="justify-content-center">
                        <Col md={6}>
                            <Card>
                                <CardBody>
                                <CardTitle><h2>Assign Target</h2></CardTitle>
                            <Form>
                                <FormGroup>
                                    <Label>Select Employee</Label>
                                    <Input type="select" name="employee">
                                        <option>EMP001 Ronish Shah</option>
                                        <option>EMP002 Jay Patel</option>
                                        <option>EMP003 Amit Singh</option>
                                    </Input>
                                </FormGroup>
                               
                                <FormGroup>
                                    <Label>Start Date</Label>
                                    <Input type="date" name="startdate" />
                                </FormGroup>
                               
                                <FormGroup>
                                    <Label>End Date</Label>
                                    <Input type="date" name="enddate" />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Type</Label>
                                    <Input type="select">
                                        <option>Lead</option>
                                        <option>Customer</option>
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Number of Target</Label>
                                    <Input type="number" min={0} />
                                </FormGroup>
                                <FormGroup>
                                    <Button type="submit" color="primary">Submit</Button>
                                    <Button type="reset">Clear</Button>
                                </FormGroup>
                            </Form>
                            </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default AssignTarget;