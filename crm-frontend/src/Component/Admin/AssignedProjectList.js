import React from 'react';
import {Button,Table, Container, Row, Col, Card, CardBody} from 'reactstrap';
const AssignedProjectList = ({assignedprojects,onDelete,projects}) => {
    return (
        
        <>
        <Container>
            <Row className="justify-content-center">
                <Col md={8}>
                    <Card>
                        <CardBody>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project Name</th>
                                        <th>Allocation Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    {
                                    assignedprojects && assignedprojects.map((project,i) => {
                                        return (
                                        <tr key={project.id}>
                                           <th scope="row">{i+1}</th>
                                            <td>{projects.map(item => {
                                                return item.id===project.projectid?item.name:''
                                                })
                                                }</td>
                                            <td>{project.allocationdate}</td>
                                            <td> <Button className="ml-2" outline id={project.id} color="danger" onClick={(e)=>onDelete(e)}>Delete</Button></td>
                                        </tr>
                                        )
                                    })
                                    }
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
      
        </Container>
        </> 
    );
}
 
export default AssignedProjectList;