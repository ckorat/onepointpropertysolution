import React from 'react';
import { Container, Row, Col, Table } from 'reactstrap';
const TargetList = ({targets}) => {
    return ( 
        <>
        <Container>
            <Row>
                <Col>
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Employee Name</th>
                            <th>Target</th>
                            <th>Target Type</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            {targets && targets.map((target,i)=>{
                                return(
                                <tr>
                                    <td>
                                        {i+1}
                                    </td>
                                <td>{target.sales}</td>
                                </tr>
                                )
                            })

                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
        </> 
    );
}
 
export default TargetList;