import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from "axios";
import { useEffect, useState } from 'react';

const Viewemployee = (props) => {
const [employeelist, setemployeelist] = useState([]);
useEffect(()=> {
    const GetData=async()=>{
      console.log("calling");
      const result = await axios(
        'http://localhost:4200/addemployee',
      );
     console.log('hello'+result.data);
     setemployeelist(result.data);
    }
    GetData();

  }, []);

const handleSubmit=(e,data1)=>{
    e.preventDefault();
    console.log("Before updation"+data1.status)
    data1.status = !data1.status;
    console.log("After updation"+data1.status);
    axios.put(`http://localhost:4200/addemployee/${data1.id}` ,data1.status) 
      .then((result) => {  
        props.history.push('/viewemployee')  
    });  
  }
  return (

    <Form>
      <div className="container">
        <div className="panel">
          <div className="title">
            <h1>List of Employees</h1>
          </div>
                  
          </div>
        </div>
        <div className="table">
        <table class="table">
          <thead>
              <tr className="table-primary">
                <th scope="col">EmployeeId</th>
                <th scope="col">First-Name</th>
                <th scope="col">Last-Name</th>
                <th scope="col">type</th>
                <th scope="col">status</th>
              </tr>
            </thead>
            <tbody>
              {console.log(employeelist)}
              {employeelist.map(data =>
                  <tr table-primary key={data.id} >
                    <td>{data.id}</td>
                    <td>{data.Fname}</td>
                    <td>{data.Lname}</td>
                    <td>{data.type}</td>
                    <Button  onClick={e=>{handleSubmit(e,data)}} color= { data.status ? "success" : "danger"} >{ data.status ? 'Active' : 'deactive'}   </Button>
                  </tr>                
              )}
            </tbody>
          </table>
        </div>
    </Form>
  );
}
export default Viewemployee;

