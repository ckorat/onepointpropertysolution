import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import React, { Component } from 'react';
import axios from "axios";
import {toast } from 'react-toastify';

class ManageEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Fname: '',
            Lname: '',
            type: '',
            Email: '',
            password: 'testing1',
            address:'',
            city:'',
            status:'1'
        }
        const initialstate={
            email: "",
            password : "",
            emailError:"",
            passwordError:""
    }
    }
   
    
    onCreate = (event) => {
        event.preventDefault();
        console.log(this.state);
    axios.post('http://localhost:4200/addemployee',this.state)
       .then((result) => {
        //    this.popup();
             console.log(result);
             toast.success("Successfully Created");
             
       })
       .catch(error=>{
            console.log(error);
            toast.error("Successfully Created");

       })
    }
   
   
    render() { 
        return ( <div className="container-fluid">
        <div className="row">
            <div className="col-lg-8 mx-auto">
                <div className="card">

                    <div className="card-body">
                        <center><h1>Add New Employee</h1></center>
                        <Form onSubmit={this.onCreate}>
                            <FormGroup>
                                <Label>First-Name:</Label>
                                <Input type="text"
                               name="fname"
                                    placeholder="Enter First Name"
                                   
                                    onChange={(event) => { this.setState({Fname: event.target.value }) }}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Last-Name:</Label>
                                <Input type="text"
                                    name="Lname"
                                  
                                    placeholder="Enter First Name"
                                    onChange={(event) => { this.setState({Lname: event.target.value }) }}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Employee-Type</Label>
                                <Input type="select"
                                    name="type"
                                  
                                    onChange={(event) => { this.setState({ type: event.target.value }) }}>
                                    <option>Agents</option>
                                    <option>Saleperson</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="text" name="Email"  placeholder="Enter Email Address " onChange={(event) => { this.setState({ Email: event.target.value }) }}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Input type="hidden" id="password" name="password" value="1" onChange={(event) => { this.setState({password: event.target.value }) }}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Address</Label>
                                <Input type="textarea" name="address" onChange={(event) => { this.setState({ address: event.target.value }) }}></Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>City</Label>
                                <Input type="select"
                                    name="city"
                                    onChange={(event) => { this.setState({ city: event.target.value }) }}>
                                    <option>Surat</option>
                                    <option>Pune</option>
                                    <option>Mumbai</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Input type="hidden" id="status" name="status" value="1" onChange={(event) => this.setState({status: event.target.value })}></Input>
                            </FormGroup>
                        
                            <FormGroup>
                                <Button type="submit" color="primary" >Submit</Button>
                                &nbsp;
                                <Button type="reset">Clear</Button>
                            </FormGroup>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    </div> );
    }
}
 
export default ManageEmployee;
