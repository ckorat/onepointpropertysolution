import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, Button, Modal, ModalBody, ModalHeader } from 'reactstrap';
import FormError from '../Common/FormError';
class InventoryForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                project: '',
                totalUnits: '',
                type: '',
                carpetarea: '',
                description: ''
            },
            errors: {},
            modal: false
        }
        this.onChange = this.onChange.bind(this);
    }
    componentWillMount() {

        if (this.props.isEditing) {
            this.setState({
                data: {
                    totalUnits: this.props.inventory.totalUnits,
                    type: this.props.inventory.type,
                    carpetarea: this.props.inventory.carpetarea,
                    description: this.props.inventory.description
                }
            });
        }
    }
    onChange = (e) => {
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })

    }

    validate = (data) => {
        const errors = {};
        if (!data.totalUnits) errors.totalUnits = "*please enter units";
        if (!data.type) errors.type = "*please select inventory type";
        if (!data.carpetarea) errors.carpetarea = "*please enter carpetarea";
        if (!data.description) errors.description = "*please enter some project description";
        return errors;
    }
    onCreate = (e) => {
        const errors = this.validate(this.state.data);
        this.setState({ errors });
        if (Object.keys(errors).length === 0) {
            this.props.handleSubmit(this.state.data, e)
        }
        e.preventDefault();

    }
    onSave = (e) => {
        const errors = this.validate(this.state.data);
        this.setState({ errors });
        if (Object.keys(errors).length === 0) {
            this.props.handleSave(this.state.data, e)
        }
        e.preventDefault();
    }

    render() {

        return (
            <Modal isOpen={this.props.modal}>
                <ModalHeader toggle={this.props.toggle}>{this.props.isEditing ? 'Edit Inventory' : 'Create Inventory'}</ModalHeader>
                <ModalBody>
                    <Form onSubmit={this.props.isEditing ? this.onSave : this.onCreate}>
                        <FormGroup>
                            <Label>Total Units</Label>
                            <Input type="number"
                                value={this.state.data.totalUnits}
                                name="totalUnits"
                                placeholder="enter units"
                                onChange={this.onChange} />
                            {this.state.errors.totalUnits && <FormError text={this.state.errors.totalUnits} />}
                        </FormGroup>
                        <FormGroup>
                            <Label>Inventory Type</Label>
                            <Input type="select"
                                name="type"
                                value={this.state.data.type}
                                onChange={this.onChange}>
                                <option></option>
                                <option>1BHK</option>
                                <option>2BHK</option>
                                <option>3BHK</option>
                                <option>SHOP</option>
                                <option>OFFICE</option>
                            </Input>
                            {this.state.errors.type && <FormError text={this.state.errors.type} />}
                        </FormGroup>
                        <FormGroup>
                            <Label>Carpet Area</Label>
                            <Input type="number"
                                name="carpetarea"
                                value={this.state.data.carpetarea}
                                placeholder="enter carpet area"
                                onChange={this.onChange} />
                            {this.state.errors.carpetarea && <FormError text={this.state.errors.carpetarea} />}
                        </FormGroup>
                        <FormGroup>
                            <Label>Description</Label>
                            <Input
                                type="textarea"
                                name="description"
                                value={this.state.data.description}
                                onChange={this.onChange} />
                            {this.state.errors.description && <FormError text={this.state.errors.description} />}
                        </FormGroup>
                        <FormGroup>
                            <Button type="submit" color="primary" >{this.props.isEditing ? 'Save' : 'Submit'}</Button>
                            <Button type="reset" onClick={this.toggle}>Clear</Button>
                        </FormGroup>
                    </Form>
                </ModalBody>
            </Modal>
        );
    }
}

export default InventoryForm;